pub struct Interval {
    begin: u8,
    end: u8,
}

impl Interval {
    pub fn new(begin: u8, end: u8) -> Interval {
        Interval { begin, end }
    }

    pub fn contains(&self, another: &Interval) -> bool {
        return if self.begin <= another.begin && self.end >= another.end {
            true
        } else {
            false
        }
    }

    pub fn overlaps(&self, another: &Interval) -> bool {
        return if (self.end <= another.end && self.end >= another.begin) || (self.begin >= another.begin && self.begin <= another.end) {
            true
        } else {
            false
        }
    }
}
use std::fs::File;
use std::io::{BufRead, BufReader, Lines};
use day_4::Interval;

fn main() {
    part_one();
    part_two();
}

fn part_one() {
    let lines = read_file("input.txt");
    let mut sum = 0;
    for line_results in lines {
        let line = line_results.unwrap();
        let (left, right) = parse_line(&line);
        let (left_interval, right_interval) = (parse_numbers(left), parse_numbers(right));
        if left_interval.contains(&right_interval) || right_interval.contains(&left_interval) {
            sum += 1;
        }
    }
    println!("{}", sum);
}

fn part_two() {
    let lines = read_file("input.txt");
    let mut sum = 0;
    for line_results in lines {
        let line = line_results.unwrap();
        let (left, right) = parse_line(&line);
        let (left_interval, right_interval) = (parse_numbers(left), parse_numbers(right));
        if left_interval.overlaps(&right_interval) || right_interval.overlaps(&left_interval) {
            sum += 1;
        }
    }
    println!("{}", sum);
}

fn read_file(filename: &str) -> Lines<BufReader<File>> {
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    reader.lines()
}

fn parse_numbers(numbers: &str) -> Interval {
    let result: Vec<u8> = numbers.split('-').map(|s| s.parse().unwrap()).collect();
    Interval::new(result[0], result[1])
}

fn parse_line(line: &str) -> (&str, &str) {
    let result: Vec<&str> = line.split(',').collect();
    (result[0], result[1])
}
